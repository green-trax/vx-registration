export const logo = {
  localhost: require("@/assets/img/eili.png"),
  innogy: require("@/assets/img/eili.png"),
  marke: require("@/assets/img/marke.svg"),
  enviam: require("@/assets/img/enviaM.jpg"),
  demo: require("@/assets/img/innogy.svg")
};
/**
 * in pdroduction indexOf should be . instead of :
 * */
export const company = {
  localhost: "innogy",
  innogy: "innogy",
  marke: "marke",
  enviam: "enviam",
  demo: "demo"
};

export const redirectUrl = {
  localhost: "https://arcane-dusk-05189.herokuapp.com/",
  innogy: "https://www.innogy-emobility.com/elektromobilitaet",
  marke: "https://www.mark-e.de/",
  enviam: "https://www.enviam.de/elektromobilitaet",
  demo: "https://www.innogy-emobility.com/elektromobilitaet"
};

export const baseUrl = "innogy";
